function [y] = E_PT(s,x)

global I L S U a k e f fc g p s0 bth Int R Lt b_u u

const = L(u)/f(u) + L(u)*e(u) - p(u,s)/R(u,s) - 1/R(u,s);
eps = p(u,s)/R(u,s) + 1/R(u,s);
epsilon = eps^a;
if(x>=bth(s))
   y = -k*epsilon*(x^a); 
else
    y = (x^a)*( (1-probability(x,bth(s))) * ( const - Lt(u,s)/((1 - x/bth(s))*fc(s)) ) - k*epsilon * (x/bth(s)) );
end


end

