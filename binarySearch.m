function [index] = binarySearch(bth, b_u, a, k, e, f, L, Lt, p, R, I, ei, ti, fc,error)

if(I>bth)
    right = bth;
else
    right = I;
end



left = 1;
flag = 0;

if(b_u >= bth)
    index = 0;
else  
    while left <= right
        mid = (left + right) / 2;
        value = Expected(mid, bth, b_u, a, k, e, f, L, Lt, p, R, ei, ti, fc);
        if (abs(value) <= error)
            index = mid;
            flag = 1;
            break;
        else
            if (value > 0)
                left = mid + 1;
            else
                right = mid - 1;
            end
        end
    end

    if flag == 0
        index = -1;
    end
    
end



end

