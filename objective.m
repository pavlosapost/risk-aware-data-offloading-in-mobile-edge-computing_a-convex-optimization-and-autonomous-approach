function [output] = objective(x, S, I, bth, b_u, a, k, e, f, L, Lt, p, R, ei, ti, fc)

total = 0;
output = 0;
for s = 1:S
    output = output + Expected(x(s), bth(s), b_u(s), a, k, e, f, L, Lt(s), p(s), R(s), ei, ti, fc(s));
    total = total + x(s);
end
output = -output + L*(I-total)*e/ei + L*(I-total)/(f*ti);
%output = -output;

end

