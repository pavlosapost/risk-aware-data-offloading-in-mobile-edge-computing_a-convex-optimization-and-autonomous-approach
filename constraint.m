function [c, ceq, gradc, gradceq] = constraint(x, S, I, bth, b_u, e, f, L, Lt, p, R, ei, ti, fc)

% Time Constraint and Energy Constraint

total2 = 0;
total3 = 0;
for s = 1:S
    %total1 = total1 + x(s);
    bt = b_u(s) + x(s);
    pr = probability(bt,bth(s));
    total2 = total2 + x(s)*( 1/R(s) + L/fc(s) + L*pr/f - L/f); %MEC(bt, bth(s), fc(s), L, Lt(s)) - L/f);
    total3 = total3 + x(s)*(p(s)/R(s) + L*pr*e - L*e);
end
c(1) = total2 + L*I/f - ti;
c(2) = total3 + L*I*e- ei;
ceq = [];

gradc = zeros(S,2);
if nargout > 2
    for s = 1:S
        gradc(s,1) = 1/R(s) + L/fc(s) + L*b_u(s)/(bth(s)*f) + 2*L*x(s)/(bth(s)*f) - L/f;
        gradc(s,2) = p(s)/R(s) + L*e*b_u(s)/bth(s) + 2*L*e*x(s)/bth(s) - L*e;
    end   
    gradceq = [];
end


end

