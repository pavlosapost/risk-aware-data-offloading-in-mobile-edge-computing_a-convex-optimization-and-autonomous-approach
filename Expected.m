function [y] = Expected(b, bth, b_u, a, k, e, f, L, Lt, p, R, ei, ti, fc)
if (b==0)
    y = 0;
else
    eps = p/(ei*R) + 1/(ti*R);
    bt = b + b_u;
    if(bt >= bth)
        y = -k*eps*(b^a); 
    else
        F = MEC(bt, bth, fc, L, Lt);        
        const = L/(f*ti)+ L*e/ei - (p/(ei*R) + 1/(R*ti) + L/(ti*F)) ;
        if(const < 0)
            test = 0; const = 0;
        end

        const = const^a;

        eps = eps^a;

        pr = probability(bt,bth);
        y = (b^a)*( (1-pr)*const -k*eps*pr );
    end
end
