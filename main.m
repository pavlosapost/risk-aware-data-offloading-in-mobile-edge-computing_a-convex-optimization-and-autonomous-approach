
%% Main Program
clear all;clc;close all;
warning off;

%global I L S U a k e f fc g p s0 bth Int R Lt b_u b_u_bar u
%load('inputs_scalability.mat');
load('inputs.mat')
%% Number of Servers and Users of the Simulation
U = 50; S = 3;

%% Prospect Theoretic Parameters
a = 0.2; k = 5;

%% Threshold values for the MEC Servers
bth = zeros(1,S);
for s = 1:S
    bth(s) = ((0.7 - 0.3)*rand + 0.3)*sum(I(1:U));
end
load('bth_50_3.mat');

%% Offloading Vector - Initiallization
b = zeros(U,S);
for u = 1:U
    b(u,1:S) = bi_input(u,1:S);
end

Rserv = 5*10^6/(128*10^3);

%% Other Parameters
LT = sum(L(1:U));
Int = zeros(1,S);
R = zeros(1,S);
Lt = zeros(1,S);
Lt(1:S) = LT;
% Aggragate Offloading Data Amounts of the Rest
b_u = zeros(1,S);
% Threshold offloading value
b_u_bar = zeros(1,S);


%% Total Offloading, Utility, Avg Utility at the initial point
Total_Offloading = [];
Avg_User_Offloading = [];
User_Offloading = [];
Users_Total_Offloading = [];
Energy_Overhead = [];
Time_Overhead = [];

Utility = [];
Probability = [];

temp = zeros(S,1);
temp2 = zeros(S,1);
for s = 1:S
    temp(s) = sum(b(:,s));
    temp2(s) = probability(temp(s),bth(s));
end
Total_Offloading = [Total_Offloading temp];
Avg_User_Offloading = [Avg_User_Offloading temp/U];
Probability = [Probability temp2];
user1 = 1;
user2 = 2;
user3 = 12;
user4 = 21;
userr = user3;

temp_utility = zeros(U,1);
temp_energy = zeros(U,1);
temp_time = zeros(U,1);
for u = 1:U
        % Calculation of the Aggregate Offloading, Interference, Transmission Rate, Application Parameter 
        for s = 1:S
            Int(s) = 0;
            l_temp = 0;
            for i = 1:U
                if(i~=u && b(i,s)~=0)
                    % Interference
                    Int(s) = Int(s) + p(i,s)*g(i,s);
                    % Application Parameter
                    l_temp = l_temp + L(i);
                end
            end
            Lt(s) = l_temp + L(u);
            % Transmission Rate
            R(s) = w*log(1 + Rserv * (p(u,s)*g(u,s))/(s0 + Int(s)));
            
            % Aggragate Offloading
            b_u(s) = sum(b(1:U,s)) - b(u,s);
        end
        temp_utility(u) = -objective(b(u,:), S, I(u), bth(1:S), b_u(1:S), a, k, e(u), f(u), L(u), Lt(1:S), p(u,1:S), R(1:S), ei(u), ti(u), fc(1:S));
        
        % Energy & Time Overhead
        energy = 0;
        time = 0;
        for s = 1:S
            bt = sum(b(:,s));
            pr = probability(bt,bth(s));
            energy = energy + b(u,s)*(p(u,s)/R(s) + L(u)*pr*e(u));
            time = time + b(u,s)*(1/R(s) + L(u)/fc(s) + L(u)*pr/f(u));
        end
        temp_energy(u) = energy + L(u)*(I(u) - sum(b(u,:)))*e(u); temp_energy(u) = temp_energy(u)/ei(u);
        temp_time(u) = time + L(u)*(I(u)-sum(b(u,:)))/f(u); temp_time(u) = temp_time(u)/ti(u);
end
Utility = [Utility temp_utility];
Energy_Overhead = [Energy_Overhead temp_energy];
Time_Overhead = [Time_Overhead temp_time];
User_Offloading = b(userr,:)';

Users_Total_Offloading = [sum(b(user1,:)); sum(b(user2,:)); sum(b(user3,:)); sum(b(user4,:))];



%% Convergence to PNE
execution_time = 0;
conv = false;
ite = 0;
check_1 = 0;
check_2 = 0;
%tic;
while(conv == false)
    ite = ite+1; disp(ite);
    
    %% Sequential Best Response Dynamics
    check_1 = 0;
    temp_utility = zeros(U,1);
    temp_energy = zeros(U,1);
    temp_time = zeros(U,1);
    tic;
    for u = 1:U
        % Calculation of the Aggregate Offloading, Interference, Transmission Rate, Application Parameter 
        for s = 1:S
            Int(s) = 0;
            l_temp = 0;
            for i = 1:U
                if(i~=u && b(i,s)~=0)
                    % Interference
                    Int(s) = Int(s) + p(i,s)*g(i,s);
                    % Application Parameter
                    l_temp = l_temp + L(i);
                end
            end
            Lt(s) = l_temp + L(u);
            % Transmission Rate
            R(s) = w*log(1 + Rserv * (p(u,s)*g(u,s))/(s0 + Int(s)));
            
            % Aggragate Offloading
            b_u(s) = sum(b(1:U,s)) - b(u,s);
        end
        
        %% Optimization Problem For Allocate The Best Response
        
        %% Step 1: Find the b_u bars for each user
         for s = 1:S
             for eps = 20:-1:-2
                error = 10^(-eps);
                b_u_bar(s) = binarySearch(bth(s), b_u(s), a, k, e(u), f(u), L(u), Lt(s), p(u,s), R(s), I(u), ei(u), ti(u), fc(s), error);
                if(b_u_bar(s) >= 0)
                    break;
                end
             end
         end
         for s = 1:S
             if(b_u_bar(s)<0)
                 test = 0;
             end
         end
         
                 
        %% Linear Inequality constraints
        A = zeros(1,S);
        A(1:S) = 1;
        ineq = I(u);
        
        %% Lower and Higher Bounds
        lb = zeros(1,S);     
        for s = 1:S
            val = bth(s) - b_u(s);
            b_u_bar(s) = min([I(u) b_u_bar(s) val]);
        end
        ub = b_u_bar;
        
        %% Initial point
        b01 = zeros(1,S);
        b02 = zeros(1,S);
        b02(1,1:S) = I(u);     

        fun = @(x)objective(x, S, I(u), bth(1:S), b_u(1:S), a, k, e(u), f(u), L(u), Lt(1:S), p(u,1:S), R(1:S), ei(u), ti(u), fc(1:S));
        nonlcon = @(x)constraint(x, S, I(u), bth(1:S), b_u(1:S), e(u), f(u), L(u), Lt(1:S), p(u,1:S), R(1:S), ei(u), ti(u), fc(1:S));
        options = optimoptions('fmincon','Display','off', 'Algorithm', 'sqp', 'SpecifyConstraintGradient', true, 'FiniteDifferenceStepSize',10);
        [x, fval1] = fmincon(fun,b01,A,ineq,[],[],lb,ub,nonlcon,options);
        temp_utility(u,1) = -fval1;             
        
        check_2 = 0; energy = 0; time = 0;       
        for s = 1:S
            perc_diff = abs(b(u,s)-x(s));          
            if(perc_diff<=0.001*b(u,s))
                check_2 = check_2 + 1;        
            end
            b(u,s) = x(s);
            bt = sum(b(:,s));
            pr = probability(bt,bth(s));
            energy = energy + b(u,s)*(p(u,s)/R(s) + L(u)*pr*e(u));
            time = time + b(u,s)*(1/R(s) + L(u)/fc(s) + L(u)*pr/f(u));          
        end
        % Energy & Time Overhead

        temp_energy(u) = energy + L(u)*(I(u) - sum(b(u,:)))*e(u); temp_energy(u) = temp_energy(u)/ei(u);
        temp_time(u) = time + L(u)*(I(u)-sum(b(u,:)))/f(u); temp_time(u) = temp_time(u)/ti(u);               
        if(check_2==S)
            check_1 = check_1 + 1;
        end
    end
    time=toc;
    Energy_Overhead = [Energy_Overhead temp_energy];
    Time_Overhead = [Time_Overhead temp_time];
    execution_time = execution_time + time;
    temp = [sum(b(user1,:)); sum(b(user2,:)); sum(b(user3,:)); sum(b(user4,:))];
    Users_Total_Offloading = [Users_Total_Offloading temp];

    % Utility Values
    Utility = [Utility temp_utility];
    
    % Total Offloading at each server
    temp = zeros(S,1);
    temp2 = zeros(S,1);
    for s = 1:S
        temp(s) = sum(b(:,s));
        temp2(s) = probability(temp(s),bth(s));
    end
    Total_Offloading = [Total_Offloading temp];
    Probability = [Probability temp2];
    
    % Average Total Offloading
    Avg_User_Offloading = Total_Offloading/U;
    
    % User Total Offloading
    
    temp = b(userr,:)';
    User_Offloading = [User_Offloading temp];
    
    
    disp(['Users at convergence: ', num2str(check_1) '']);
    if(check_1 >= U-1)
        conv = true;
    end
end
%time=toc;
disp(['Time: ', num2str(execution_time) ' seconds'])

%% Plots

step = 1;
stop = ite+1;

%% User Offloading, Somes Users' Offloading, Total Average Offloading

figure();
title('Users 25 offloading')
hold on;
for s = 1:S
    plot(1:step:stop,User_Offloading(s,1:step:stop));
end
hold off;


figure();
title('Users Total Offloading')
hold on;
for i = 1:4
    plot(1:step:stop,Users_Total_Offloading(i,1:step:stop));
end


%% Some Users Utility



figure();
title('Users Utility')
hold on;
for i = [user1 user2 user3 user4]
    plot(1:step:stop,Utility(i,1:step:stop));
end




%%  Average Utility, Average Overhead
Overhead = zeros(U,stop);

for i = 1:U
    Overhead(i,:) = Time_Overhead(i,:) + Energy_Overhead(i,:);
end


Avg_Utility = zeros(1,stop);
Avg_Overhead = zeros(1,stop);
for i = 1:(ite+1)
    Avg_Utility(i) = sum(Utility(:,i))/U;
    Avg_Overhead(i) = sum(Overhead(:,i))/U;
end
figure(); 
axes('box','on'); 
hold on;  
yyaxis left 
plot(1:step:stop,Avg_Utility);
ylabel('Utility'); 
yyaxis right
plot(1:step:stop,Avg_Overhead);
ylabel('Overhead'); 
grid on; 
hold off; 
set(gca,'FontWeight','bold');
% figure();
% title('Avg Users Offloading to each server')
% hold on;
% for s = 1:S
%     plot(1:step:stop,Avg_User_Offloading(s,1:step:stop));
% end
% hold off;




%% Utility and Average Utility

% figure();
% title('Users 2 Utility');
% hold on;
% plot(1:step:stop,Utility(user2,1:step:stop));
% hold off;


%% User Overheads
energy_constraint(1:(ite+1)) = ei(userr);
time_constraint(1:(ite+1)) = ti(userr);


figure(); 
axes('box','on'); 
hold on;  
yyaxis left 
plot(1:step:stop,ei(userr)*Energy_Overhead(userr,1:step:stop));
plot(1:step:stop,energy_constraint);
ylabel('Energy'); 
yyaxis right
plot(1:step:stop,ti(userr)*Time_Overhead(userr,1:step:stop));
plot(1:step:stop,time_constraint);
ylabel('Time'); 
grid on; 
hold off; 
set(gca,'FontWeight','bold');




%% Servers' Total Received Offloading and Probability of Failure

figure();
title('Total Offloading to the Servers')
hold on;
for s = 1:S
    plot(1:step:stop,Total_Offloading(s,1:step:stop));
end

axes('Position',[.7 .7 .2 .2]);
box on
hold on;
for s = 1:S
    plot(1:step:stop,Probability(s,1:step:stop));
end
grid on;
hold off;
hold off;
set(gca,'FontWeight','bold');



figure();
title('Probability of Failure');
hold on;
for s = 1:S
    plot(1:step:stop,Probability(s,1:step:stop));
end
hold off;



%% Threshold Values of the Server
acc = 0;
for s = 1:S
    acc = acc + fc(s)*bth(s)/(sum(d(:,s))/U);
end
figure();
x = 1:1:3;
factor = [(fc(1)*bth(1)/(sum(d(:,1))/U))/acc (fc(2)*bth(2)/(sum(d(:,2))/U))/acc (fc(3)*bth(3)/(sum(d(:,3))/U))/acc];
b = bar(diag(factor), 'stacked');
set(gca, 'xticklabel', {'Server 1', 'Server 2', 'Server 3'});
%ylabel('Factor: $\mathbf{\frac{\lambda_i*I_i}{d_i*f^l_i}}$', 'Interpreter', 'latex');
ylabel('Factor');



%% Computation Capabilities
figure();

x = 1:1:3;
factor = [fc(1) fc(2) fc(3)];
b = bar(diag(factor), 'stacked');
set(gca, 'xticklabel', {'Server 1', 'Server 2', 'Server 3'});
%ylabel('Factor: $\mathbf{\frac{\lambda_i*I_i}{d_i*f^l_i}}$', 'Interpreter', 'latex');
ylabel('Computation Capability');



%% Average Distance
figure();

x = 1:1:3;
factor = [sum(d(:,1))/U sum(d(:,2))/U sum(d(:,3))/U];
b = bar(diag(factor), 'stacked');
set(gca, 'xticklabel', {'Server 1', 'Server 2', 'Server 3'});
%ylabel('Factor: $\mathbf{\frac{\lambda_i*I_i}{d_i*f^l_i}}$', 'Interpreter', 'latex');
ylabel('Normalized Average Distance');

%% Bar Graph

figure();
d1=sum(d(:,1))/U;
d2=sum(d(:,2))/U;
d3=sum(d(:,3))/U;
dtotal= d1+d2+d3;
d1=d1/dtotal;
d2=d2/dtotal;
d3=d3/dtotal;
factor = [d1 d2 d3];
b = bar(diag(factor), 'stacked');
set(gca, 'xticklabel', {'Server 1', 'Server 2', 'Server 3'});
ylabel('Normalized Average Distance');

figure();
ftotal = fc(1)+fc(2)+fc(3);
f1 = fc(1)/ftotal;
f2 = fc(2)/ftotal;
f3 = fc(3)/ftotal;
factor = [f1 f2 f3];
b = bar(diag(factor), 'stacked');
set(gca, 'xticklabel', {'Server 1', 'Server 2', 'Server 3'});
ylabel('Normalized Computation Capability');

figure();
btotal = bth(1)+bth(2)+bth(3);
b1 = bth(1)%/btotal;
b2 = bth(2)%/btotal;
b3 = bth(3)%/btotal;
factor = [b1 b2 b3];
b = bar(diag(factor), 'stacked');
set(gca, 'xticklabel', {'Server 1', 'Server 2', 'Server 3'});
ylabel('Threshold value');

y1 = (f1+b1)/d1;
y2 = (f2+b2)/d2;
y3 = (f3+b3)/d3;
 y = y2;
 y2 = y3;
 y3 = y;
figure();
axes;
factor = [y1 y2 y3];
b = bar(diag(factor), 'stacked');
set(gca, 'xticklabel', {'Server 1', 'Server 2', 'Server 3'});
ylabel('Incentive Factor');
%ylabel('$ \frac{\frac{\bar{b}_s}{\sum_{j \in \mathbb{S}}{\bar{b}_j}} + \frac{F_s}{\sum_{j\in \mathbb{S}}{F_j}}}{\frac{\sum_{i\in \mathbb{D}}{d_{i,s}}}{\sum_{j\in \mathbb{S}}{\sum_{i\in \mathbb{D}}{d_{i,j}}}}}$','fontsize',14,'interpreter','latex')
