%% Users & Servers
U = 500; % Users
S = 30; % Servers

%% Distances & Channel Gains
d = zeros(U,25);
g = zeros(U,25);
a = 10; b = 100;

coverage_areas = zeros(1,S);
for s = 1:S
    coverage_areas(s) = 300*rand + 100;
end


for u = 1:U
    for s = 1:S
        d(u,s) = (100-a)*rand + a;
        g(u,s) = 1/(d(u,s)^2);
    end
end

%% Users Power Control
p = zeros(U,S);
for u = 1:U
    for s = 1:S
        p(u,s) = 0.1 * (d(u,s)^2/100^2);
    end
end


%% Users' Computation Capabilities
f = zeros(1,U);
for u = 1:U
    f(u) = randi([1 10],1,1)*10^8;
end

%% Users' Energy Consumption per CPU Cycle
e = zeros(1,U);
for u = 1:U
    e(u) = 10^(-9);
end

%% Input Data into KBytes
I = zeros(1,U);
for u = 1:U
    I(u) = randi([1000 5000],1,1) * 10^3;
end

%% Users' Application Parameters
L = zeros(1,U);
for u = 1:U
    a = 1; b = 5;
    L(u) = ((b-a)*rand + a)*10^3;
end

%% Initiallization Offloading
bi_input = zeros(U,S);
for u = 1:U
    temp = randi([0 I(u)],1,1);
    bi_input(u,:) = randfixedsum(S,1,temp,0,temp)';
end


%% MEC Servers' Computation Capabilities
fc = zeros(1,S);
for s = 1:S
    fc(s) = randi([1000 4000],1,1)*10^9;
end

%% Communication Parameters
w = 5*10^6; % bandwidth
s0 = 10^(-13); % background noise


%load('inputs.mat');
ei = zeros(1,U);
ti = zeros(1,U);


for u = 1:U
    val1 = L(u)*I(u)*e(u);
    val2 = L(u)*I(u)/f(u);
    
    ei(u) = (rand + 1.5)*val1;
    ti(u) = (rand + 1.5)*val2;
end



